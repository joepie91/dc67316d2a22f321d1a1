<?php

echo("Before the first file is read.");
$fileContents = file_get_contents("sample.txt"); // You can't do anything while the file is being read, your script is 'stuck'.
echo("After the first file has completed reading.");

echo("Before the second file is read.");
$fileContents = file_get_contents("sample2.txt"); // You can't do anything while the file is being read, your script is 'stuck'.
echo("After the second file has completed reading.");


/* Output always looks like this:
Before the first file is read.
After the first file has completed reading.
Before the second file is read.
After the second file has completed reading.
*/