console.log("Before the first file is read.");
hypotheticalFileGetContents("sample.txt", function(fileContents){
	// fileContents now contains the file contents, this function is only called when the file read in the background has finished
	console.log("After the first file has completed reading.");
});

// You've now told it to start the first read, but it won't 'block' your script execution. It will do the read in the background, and immediately move on with the rest of your code.

console.log("Before the second file is read.");
hypotheticalFileGetContents("sample2.txt", function(fileContents){
	// fileContents now contains the file contents, this function is only called when the file read in the background has finished
	console.log("After the second file has completed reading.");
});

/* Output could look something like this:
Before the first file is read.
Before the second file is read.
After the first file has completed reading.
After the second file has completed reading.
*/